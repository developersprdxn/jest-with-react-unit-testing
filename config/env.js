'use strict';

// Grab NODE_ENV and REACT_APP_* environment variables and prepare them to be
// injected into the application via DefinePlugin in Webpack configuration.

var REACT_APP = /^REACT_APP_/i;

function getClientEnvironment(publicUrl) {

  if (process.env.Environment === 'qa') {
    process.env.API_URL = "http://kiddoo-qa-api.eu-west-1.elasticbeanstalk.com";
    process.env.FB_APP_ID = "1875710246036503";
    process.env.STRIPE_PK = "pk_test_5lfE76J0E5JKpFHZX3jNkrIh";
  } else if (process.env.Environment === 'dev') {
    process.env.API_URL = "http://kiddoo-dev-api.eu-west-1.elasticbeanstalk.com";
    process.env.FB_APP_ID = "1875710599369801";
    process.env.STRIPE_PK = "pk_test_5lfE76J0E5JKpFHZX3jNkrIh";
  } else if (process.env.Environment === 'release') {
    process.env.API_URL = "https://api.kidadl.com";
    process.env.FB_APP_ID = "1772612443012951";
    process.env.STRIPE_PK = "pk_live_PNhYQ8gKUQYvqP5n1kGWA5RK";
  } else {
    process.env.API_URL = "http://192.168.1.160:8081"
    process.env.FB_APP_ID = "1875710759369785";
    process.env.STRIPE_PK = "pk_test_5lfE76J0E5JKpFHZX3jNkrIh";
  }

  var raw = Object
    .keys(process.env)
    .filter(key => REACT_APP.test(key))
    .reduce((env, key) => {
      env[key] = process.env[key];
      return env;
    }, {
      // Useful for determining whether we’re running in production mode.
      // Most importantly, it switches React into the correct mode.
      'NODE_ENV': process.env.NODE_ENV || 'development',
      // Useful for resolving the correct path to static assets in `public`.
      // For example, <img src={process.env.PUBLIC_URL + '/img/logo.png'} />.
      // This should only be used as an escape hatch. Normally you would put
      // images into the `src` and `import` them in code to get their paths.
      'PUBLIC_URL': publicUrl,
      'API_URL': process.env.API_URL,
      'FB_APP_ID': process.env.FB_APP_ID,
      'STRIPE_PK': process.env.STRIPE_PK
    });
  // Stringify all values so we can feed into Webpack DefinePlugin
  var stringified = {
    'process.env': Object
      .keys(raw)
      .reduce((env, key) => {
        env[key] = JSON.stringify(raw[key]);
        return env;
      }, {})
  };

  return { raw, stringified };
}

module.exports = getClientEnvironment;

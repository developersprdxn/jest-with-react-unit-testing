# README #

## What is this repository for? ##

This repository consist test scripts for React components, Actions and Reducers.
Which will help people to understand How to write test scripts for React components, Actions and Reducers using JEST.

# Table of Contents #

* [How do I get set up?](#markdown-header-how-do-i-get-set-up)
* [Folder Structure](#markdown-header-folder-structure)
* [Command line Interface](#markdown-header-command-line-interface)
* [For What we writes a test script?](#markdown-header-for-what-we-writes-a-test-script)
* [Description of written test scripts](#markdown-header-description-of-written-test-scripts)
   - [Components](#markdown-header-1-components)
   * [Actions](#markdown-header-2-acions)
   * [Reducers](#markdown-header-3-reducers)
* [Some Useful things for Writing Tests](#markdown-header-some-useful-things-for-writing-tests)
   * [Shallow](#markdown-header-shallow)
   * [Enzyme](#markdown-header-enzyme)  
   * [Mount](#markdown-header-mount)
## How do I get set up? ##

1. Clone repository using command: **git clone https://developersprdxn@bitbucket.org/developersprdxn/jest-with-react-unit-testing.git**

2. Inside project folder open terminal and run command: **npm install**
(Which will install all required dependencies)

3. Now you are ready to go
```
 If you want to run all test together run command: npm test
 If you want to run single test script run command: npm test filename (eg. npm test app.test.js)

```

## Command Line Interface ##

**For running individual test script:**

![run-individual-test-1.gif](https://bitbucket.org/repo/Gg6K5xb/images/1245716288-run-individual-test-1.gif)

**For running all test script together:**

![run-all-test-1.gif](https://bitbucket.org/repo/Gg6K5xb/images/3735262853-run-all-test-1.gif)


## Folder Structure ##
**
Test script written for components you will find inside:**

```
-_test_
  -components
    -weather-info
      -weather-info.test.js
    -link.test.js (Snapshot testing)
    -checkbox-with-label.test.js (DOM Testing)
```
 
**Test script written for actions you will find inside:**
```
-_test_
   -actions
     -weather-info
       -weather.test.js

```
**
Test script written for reducer you will find inside:**
```
-_test_
  -reducers
    -reducer-weather-info
      -reducer-weather-info.test.js

```

## For What we writes a test script? ##

Basically, we are writing unit test script to check the behavior.

for example: If there is function **sum** which takes two parameters and returns a sum of that two numbers. So in unit testing what I'll check: When I passes two numbers (2,3) to sum function it should return 5(i.e sum of that two numbers) then only this test will pass otherwise it'll fail. Same thought I utilized for writing following unit test scripts.

##Description of written test scripts
 
**Short decryption about written unit test script for Components, Actions and Reducers**

###1. Components:
As components are there to render the props which we passed to it. So here we check following things:
  
**1.1 weather-info.test.js (Path: ```_test_``` -> Componennts -> weather-info -> weather-info.test.js )**
```
    i) First check whether weather-info component is exist, if YES then PASS the script otherwise FAIL.
   ii) All required props are passed to the component.
  iii) Check when all required props are passed to the component, it rendering in expected manner.

```

**1.2 link.test.js (Path: ```_test_``` -> components -> link.test.js)**

Above test script will help you to understand **Snapshot testing**.

Here I used React's test renderer and Jest's snapshot feature to interact with the component and capture the rendered output and create a snapshot file.

When you run command:  **npm test link.test.js**  snapshot file will be created inside a ``` _test_``` -> components -> _snapshots_ -> link.test.js.snap.

The next time you run the tests, the rendered output will be compared to the previously created snapshot. The snapshot should be committed along code changes. When a snapshot test fails, you need to inspect whether it is an intended or unintended change. If the change is expected you can invoke Jest with **jest -u** to overwrite the existing snapshot.
 
**1.3 checkbox-with-label.test.js (Path: ```_test_``` -> components -> checkbox-with-label.test.js)**

Above test script will help you to understand **DOM testing**.

to execute this script run command: **npm test checkbox-with-label.test.js**



### 2. Actions: 
Action creators are functions which return plain objects. When testing action creators we want to test whether the correct action creator was called and also whether the right action was returned, So here we check following things:

**2.1 weather-info.test.js (Path: ```_test_``` -> actions -> weather-info -> weather.test.js)**

```
     i) When called requestWeatherInfo(), check whether it returning expected type and data.
    ii) Same check for fetchFailedWeatherInfo() and receivedWeatherInfo().
   iii) When made API call in a action should return expected types and data.  


```

### 3. Reducers: 
A reducer should return the new state after applying the action to the previous state, and that's the behavior tested below.

**3.1 reducer-weather-info-test.js (Path: ```_test_``` -> reducers -> reducer-weather-info -> reducer-weather-info-test.js)**
```
     i) Should return the initial state.
    ii) Should handle request for weather info and accordingly return new state.
   iii) Should handle failed condition for fetching weather info and accordingly return new state.
    iv) Should handle success condition for fetching weather info and accordingly return new state.

```

## Some Useful things for Writing Tests ##

To create tests, add ``` it() (or test()) ``` blocks with the name of the test and its code. You may optionally wrap them in ```describe()``` blocks for logical grouping but this is neither required nor recommended.

Jest provides a built-in ```expect()``` global function for making assertions. A basic test could look like this:

```
import sum from './sum';

it('sums numbers', () => {
  expect(sum(1, 2)).toEqual(3);
  expect(sum(2, 2)).toEqual(4);
});

```

All expect() matchers supported by Jest are extensively documented here.
You can also use ```jest.fn() and expect(fn).toBeCalled()``` to create �spies� or mock functions.


## Shallow and Enzyme used in component testing. ##

### Shallow: ###
Shallow rendering is useful to constrain yourself to testing a component as a unit, and to ensure that your tests aren't indirectly asserting on behavior of child components.

For more information: [Shallow Documentation](http://airbnb.io/enzyme/docs/api/shallow.html)

### Enzyme: ###
Enzyme is a JavaScript Testing utility for React that makes it easier to assert, manipulate, and traverse your React Components' output.

For more information: [Enzyme Documentation](http://airbnb.io/enzyme/)

### Mount ###
A method that re-mounts the component. This can be used to simulate a component going through an unmount/mount lifecycle.

For more informatin: [Mount Documentation](http://airbnb.io/enzyme/docs/api/ReactWrapper/mount.html)
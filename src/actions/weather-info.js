import axios from 'axios';
import { timeout } from '../utils/domUtils';

import {
	ROOT_URL,
  API_KEY,
  REQ_TIMEOUT,
  REQUEST_WEATHER_INFO,
  SUCCESS_WEATHER_INFO,
  FAILED_WEATHER_INFO,
  NETWORK_FAILURE_MESSAGE
} from './action-types';

export function requestWeatherInfo() {
  return {
    'type': REQUEST_WEATHER_INFO,
    'isWeatherInfoFetching': true
  };
}

export function receivedWeatherInfo(data) {
  return {
    'type': SUCCESS_WEATHER_INFO,
    'isWeatherInfoFetching': false,
    data
  };
}

export function fetchFailedWeatherInfo(data) {
  return {
    'type': FAILED_WEATHER_INFO,
    'isWeatherInfoFetching': false,
    'errMessage': data
  };
}

//get weather info of mumbai city
export function getWeatherInfo() {

  return dispatch => {

    dispatch(requestWeatherInfo());

    return timeout(REQ_TIMEOUT, axios.get(`${ROOT_URL}${API_KEY}`)).then((response) => {
        if(response.status === 200) {
          dispatch(receivedWeatherInfo(response.data));
          return response;
        }
      }).catch(err => {
        if(err.data){
          dispatch(fetchFailedWeatherInfo(err.data));
        }else {
          dispatch(fetchFailedWeatherInfo(NETWORK_FAILURE_MESSAGE));
        }
        return err;
      });
  };
}



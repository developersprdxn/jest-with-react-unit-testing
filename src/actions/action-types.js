export const REQ_TIMEOUT = 10000;

/* API KEY for connecting with weather api */
export const API_KEY = '596cad5e0bd7ce1fecaf30719643bd52';

/* weather api */
export const ROOT_URL = 'http://api.openweathermap.org/data/2.5/weather?q=Mumbai&APPID=';

export const NETWORK_FAILURE_MESSAGE = 'Unable to connect! please check your internet connection';

export const REQUEST_WEATHER_INFO = 'REQUEST_WEATHER_INFO';
export const SUCCESS_WEATHER_INFO = 'SUCCESS_WEATHER_INFO';
export const FAILED_WEATHER_INFO = 'FAILED_WEATHER_INFO';
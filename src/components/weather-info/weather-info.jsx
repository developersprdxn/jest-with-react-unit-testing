import React, { Component } from 'react';

import { connect } from 'react-redux';

import { getWeatherInfo } from '../../actions/weather-info';

import './style/weather-info.scss';

export class WeatherInfo extends Component {
  
  componentWillMount() {
    this.props.getWeatherInfo();
  }

  render() {
    const {
      fetchedWeatherInfo
    } = this.props;

    const {
      errMessage,
      weatherInfo
    } = fetchedWeatherInfo

    if(errMessage) {
      return <p>{errMessage}</p>
    }

    if(!weatherInfo) {
      return <p>Loading...</p>
    }

    return (
      <div className="weather-info-container">
        <h1>Weather Information of Mumbai city</h1>
        <div>
          <span className="title">Current Weather Status: </span> 
          <span>{weatherInfo.weather[0].main}</span>
        </div>
         <div>
          <span className="title">Type: </span> 
          <span>{weatherInfo.weather[0].description}</span>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const {
    fetchedWeatherInfo
  } = state;

  return {
    fetchedWeatherInfo
  };
}

export default connect(mapStateToProps, { getWeatherInfo })(WeatherInfo);
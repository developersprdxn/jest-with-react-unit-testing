import React, { Component } from 'react';

import './style/app.scss';

class App extends Component {

  render() {
    return (
      <div className='App'>
        <div className='ui text container'>
          {/* all children components goes here */}
          {
            this.props.children
          }
        </div>
      </div>
    );
  }
}

export default App;
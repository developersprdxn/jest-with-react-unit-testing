import App from '../components/app/App.js';
import WeatherInfo from '../components/weather-info/weather-info';

const routes = {

  /* root route and its component */
  path: '/',
  component: App,
  indexRoute: {
    component: WeatherInfo
  }

};

export default routes;
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';

/* store from redux */
import store from './reducers/index';

/* import all routes */
import routes from './routes/index';

/* required style */
// import './components/common/assets/styles/index.css';
// import '../semantic/dist/semantic.min.css';

/* render our app */
ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory} routes={routes} />
  </Provider>,
  document.getElementById('root')
);

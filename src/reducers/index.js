import { combineReducers, createStore, applyMiddleware } from "redux";

import ReduxThunk from 'redux-thunk';

import { fetchedWeatherInfo } from './reducer-weather-info';

/* combine all reducer */
const reducers = combineReducers({
  fetchedWeatherInfo
});

/* create store and apply required middleware */
export default createStore(reducers, applyMiddleware(ReduxThunk));


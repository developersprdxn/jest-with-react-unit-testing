import {
  REQUEST_WEATHER_INFO,
  SUCCESS_WEATHER_INFO,
  FAILED_WEATHER_INFO
} from '../actions/action-types';

const INITIAL_STATE = {
  'weatherInfo': null,
  'isWeatherInfoFetching': false,
  'errMessage': ''
}

export function fetchedWeatherInfo(state=INITIAL_STATE, action) {
  switch(action.type) {
    case REQUEST_WEATHER_INFO:
      return Object.assign({}, state, {
        'isWeatherInfoFetching': action.isWeatherInfoFetching
      });
    case SUCCESS_WEATHER_INFO:
      return Object.assign({}, state, {
        'isWeatherInfoFetching': action.isWeatherInfoFetching,
        'weatherInfo': action.data,
        'errMessage': ''
      });
    case FAILED_WEATHER_INFO:
      return Object.assign({}, state, {
        'isWeatherInfoFetching': action.isWeatherInfoFetching,
        'errMessage': action.errMessage
      });

    default:
      return state;
  }
}
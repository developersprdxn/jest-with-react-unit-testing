import expect from 'expect';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import * as actions from '../../../src/actions/weather-info';
import * as types from '../../../src/actions/action-types';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('async actions', () => {
  afterEach(() => {
    nock.cleanAll()
  });

  describe('search actions for finding weather info', () => {
    it('should create an action to state that fetching of weather info has begun', () => {
      const expectedAction = {
        'type': types.REQUEST_WEATHER_INFO,
        'isWeatherInfoFetching': true
      };
      expect(actions.requestWeatherInfo()).toEqual(expectedAction);
    });

    it('should create an action to state that fetching of weather info has failed', () => {
      const expectedAction = {
        type: types.FAILED_WEATHER_INFO,
        'isWeatherInfoFetching': false,
        'errMessage': 'Search has failed'
      };
      expect(actions.fetchFailedWeatherInfo('Search has failed')).toEqual(expectedAction);
    });
    
    it('should create an action to state that fetching of weather info has completed', () => {

      const store = mockStore();

      return store.dispatch(actions.getWeatherInfo()).then(res => {

      let expectedAction = null;

      if(res.status === 200) {
         expectedAction = [
          {
            'type': types.REQUEST_WEATHER_INFO,
            'isWeatherInfoFetching': true
          },
          {
            'type': types.SUCCESS_WEATHER_INFO,
            'isWeatherInfoFetching': false,
            'data': res.data
          }
        ]
      } else {
        expectedAction = [
          {
            'type': types.REQUEST_WEATHER_INFO,
            'isWeatherInfoFetching': true
          },
          {
            'type': types.FAILED_WEATHER_INFO,
            'isWeatherInfoFetching': false,
            'errMessage': res.data
          }
        ]
      }
      expect(store.getActions()).toEqual(expectedAction);
      }).catch(err => {
          let expectedFailure = [
            {
              'type': types.REQUEST_WEATHER_INFO,
              'isWeatherInfoFetching': true
            },
            {
              type: types.FAILED_WEATHER_INFO,
              'isWeatherInfoFetching': false,
              'errMessage': NETWORK_FAILURE_MESSAGE
            }
          ]

        expect(store.getActions()).toEqual(expectedFailure);
      })

    });
  });
});

import React from 'react';
import 'jsdom-global/register';
import { mount, shallow } from 'enzyme';

import { WeatherInfo } from '../../../src/components/weather-info/weather-info';

function setup() {

  const props = {
    fetchedWeatherInfo: {
    	weatherInfo: {
    		weather : [{
    			main: 'Rain',
    			description: 'Heavy'
    		}]
    	}
    },
    getWeatherInfo: jest.fn()
  }

  const enzymeWrapper = mount(<WeatherInfo { ...props }/>);

  return {
    props,
    enzymeWrapper
  }
}

describe('components', () => {
	 const { enzymeWrapper } = setup();
    it('Check is component available', () => {
      expect(enzymeWrapper).toHaveLength(1);
    });

    it('Check fetchedWeatherInfo prop', () => {
    	expect(enzymeWrapper.props().fetchedWeatherInfo).toEqual({"weatherInfo": {"weather": [{"description": "Heavy", "main": "Rain"}]}});
    })

    it('Check if props are updated then render correctly', ()=>{
      expect(enzymeWrapper.text()).toEqual('Weather Information of Mumbai cityCurrent Weather Status: RainType: Heavy');
    });
})
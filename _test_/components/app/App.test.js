import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import App from '../../../src/components/app/App';

/* Test to check the app lenght of App */
describe('Show App', () => {
  it('Check whether app is present', () => {
    const component = shallow(<App />);
    expect(component).toHaveLength(1);
  });
});


import { fetchedWeatherInfo } from '../../../src/reducers/reducer-weather-info';
import * as types from '../../../src/actions/action-types';

describe('weather info reducer', () => {

  /* test initial state */
  it('should return the initial state', () => {
    expect(fetchedWeatherInfo(undefined, {})).toEqual(
      {
        'weatherInfo': null,
			  'isWeatherInfoFetching': false,
			  'errMessage': ''
      }
    )
  })

  it('should handle request for weather info', () => {
    expect(
      fetchedWeatherInfo([], {
        type: types.REQUEST_WEATHER_INFO,
        isWeatherInfoFetching: true
      })
    ).toEqual(
      {
        isWeatherInfoFetching: true
      }
    )
  })

  it('should handle failed condition for fetching weather info', () => {
     expect(
      fetchedWeatherInfo([], {
        type: types.FAILED_WEATHER_INFO,
        'isWeatherInfoFetching': false,
        'errMessage': types.NETWORK_FAILURE_MESSAGE
      })
    ).toEqual(
      {
        'isWeatherInfoFetching': false,
        'errMessage': types.NETWORK_FAILURE_MESSAGE
      }
    )
  })

   it('should handle success condition for fetching weather info', () => {
     expect(
      fetchedWeatherInfo([], {
        type: types.SUCCESS_WEATHER_INFO,
        'isWeatherInfoFetching': false,
        'data': {
          weather: {
            main: 'Rain',
            description: 'heavy'
          }
        }
      })
    ).toEqual(
      {
        'isWeatherInfoFetching': false,
        'weatherInfo': {
          weather: {
            main: 'Rain',
            description: 'heavy'
          }
        },
        'errMessage': ''
      }
    )
  })
})